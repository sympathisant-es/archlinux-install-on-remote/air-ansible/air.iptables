Ansible Archlinux Iptables
==========================

A role to set basic iptable config

Tested and Used on ArchLinux but it May work on any Linux

Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/converge.yml)


License
-------

[License](LICENSE)


Todo
----
- Should check (again) https://wiki.archlinux.org/index.php/Simple_stateful_firewall
- Be Kind
